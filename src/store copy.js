import { createStore } from 'vuex';
import createPersistedState from "vuex-persistedstate";

export default createStore({
  plugins: [createPersistedState()],
  state: {
    cart: [], 
  },
 mutations: {
  addToCart(state, product) {
    const existingItem = state.cart.find(cartItem => cartItem.product.id === product.id);
    if (existingItem) {
      existingItem.quantity++;
    } else {
      state.cart.push({ product, quantity: 1 });
    }
   },
   updateCartItem(state, updatedItem) {
      const index = state.cart.findIndex(item => item.product.id === updatedItem.product.id);
      if (index !== -1) {
        state.cart[index] = updatedItem;
      }
    },
  removeFromCart(state, index) {
    state.cart.splice(index, 1);
  },
  incrementQuantity(state, index) {
    state.cart[index].quantity++;
  },
  decrementQuantity(state, index) {
    if (state.cart[index].quantity > 1) {
      state.cart[index].quantity--;
    }
  },
},

  actions: {
  },
  getters: {
    cart: state => state.cart,
    totalCost: state => {
      return state.cart.reduce((total, cartItem) => total + cartItem.product.price * cartItem.quantity, 0);
    },
  },
});
